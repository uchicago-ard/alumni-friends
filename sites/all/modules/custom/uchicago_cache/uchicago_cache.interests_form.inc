<?php

/**
 * Makes the Alumni and Friends customize experience form.
 *
 *
 */
function uchicago_cache_form($form, &$form_state) {

  require_once(DRUPAL_ROOT . '/includes/locale.inc');
  // Get previous values stored as in a cookie.
  $cached_interests = uchicago_cache_get_session_value('interests');
  $cached_affiliation = uchicago_cache_get_session_value('affiliation');
  $cached_loc = uchicago_get_cached_loc();
  //dpm($cached_interests);
  //dpm($cached_affiliation);

  $country_list = country_get_list();

  // Location.
  $form['location'] = array(
    '#type' => 'fieldset',
    '#title' => t('Set your region'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $form['location']['country_code'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => $country_list,
    '#default_value' => isset($cached_loc['country_code']) ? $cached_loc['country_code'] : 'US',
  );

  $form['location']['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#default_value' => isset($cached_loc['address']) ? $cached_loc['address'] : (isset($cached_loc['postal_code']) ? $cached_loc['postal_code'] : ''),
    '#description' => 'Enter any combination of address, city, state/provence, or postal code.',
    '#input_group' => TRUE,
    '#field_suffix' => '<i class="fa fa-globe"></i>',
  );

  // Alumni Year and Info.
  $form['affiliation-fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tell us about yourself'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  // Get all affiliation taxonomy terms.
  $vocab = uc_taxonomy_get_nested_tree(6);

  foreach ($vocab as $tid => $term) {
    $options[$tid] = $term->name;
  }

  $form['affiliation-fieldset']['affiliation'] = array(
    '#type' => 'select',
    '#title' => t('I am a ...'),
    '#options' => $options,
    '#default_value' => $cached_affiliation['affiliation'],
  );

  $form['affiliation-fieldset']['year'] = array(
    '#type' => 'textfield',
    '#title' => t('Class Year'),
    '#default_value' => $cached_affiliation['year'],
    '#states' => array(
      'visible' => array(
        // Only show to alumni TID 49.
        '#edit-affiliation-fieldset-affiliation' => array('value' => '49'),
      ),
    ),
  );

  // Return all top level taxonomy terms from interests vocabulary.
  $vocab = uc_interests_taxonomy_get_nested_tree();

  // Interests.
  $form['interest-fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Set your interests'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  foreach ($vocab as $tid => $term) {
    //dpm($tid);
    //dpm($term);

    // Build Form Item.
    $form['interest-fieldset'][$tid] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('interest-container')),
    );
    $form['interest-fieldset'][$tid][$tid] = array(
      '#type' => 'checkbox',
      '#title' => t($term->name),
      '#attributes' => array('class' => array('interest-parent')),
    );

    // If child terms exist, loop through them.
    if (isset($term->children)) {
      $children = $term->children;
      $children_list = "<ul class='child-list'>";
      foreach ($children as $term) {
        $children_list .= "<li>" . $term->name . "</li>";
      }
      $children_list .= "</ul>";

      $form['interest-fieldset'][$tid]['markup'] = array(
        '#type' => 'markup',
        '#markup' => $children_list,
      );
    }
  }


  // If previous selections were made, pre-check the boxes.
  if (isset($cached_interests)) {
    foreach ($cached_interests as $tid1 => $value1) {
      foreach ($value1 as $tid2 => $value2) {
        if ($value2 == 1) {
          $form['interest-fieldset'][$tid1][$tid2]['#attributes'] = array('checked' => 'checked');
        }
      }
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Preferences'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), '#',
      array(
        'attributes' => array(
          'class' => 'ctools-close-modal',
        ),
        'external' => TRUE,
      )
    ),
  );

  return $form;
}


function uchicago_cache_form_validate($form, &$form_state) {
}


/**
 * Save data from form back to session cookie.
 */
function uchicago_cache_form_submit($form, &$form_state) {
  // Calculate GPS location from submitted values.
  $address = $form_state['values']['location']['address'] . " " . $form_state['values']['location']['country_code'];
  $point = geocoder("google", $address);
  $form_state['values']['location']['latitude'] = $point->coords[1];
  $form_state['values']['location']['longitude'] = $point->coords[0];

  $cached_interests = uchicago_cache_get_session_value('interests');
  $cached_affiliation = uchicago_cache_get_session_value('affiliation');

  // Save user count information to taxonomy entities.
  // Check if saved values in cookie exist.
  if (!isset($cached_interests)) {
    // If FALSE, must be new user submission.
    foreach ($form_state['values']['interest-fieldset'] as $tid => $term) {
      uchicago_cache_user_count_check($term, $tid);
    }
  }
  else {
    // If TRUE, check value by value to see what has changed, if anything.
    foreach ($form_state['values']['interest-fieldset'] as $tid => $term) {
      uchicago_cache_user_count_check($term, $tid, $cached_interests[$tid]);
    }
  }
  // Check if affiliation is not set, or new value does not match cached value.
  if (!isset($cached_affiliation) || $form_state['values']['affiliation-fieldset']['affiliation'] !== $cached_affiliation['affiliation']) {
    uchicago_cache_user_count_increment($form_state['values']['affiliation-fieldset']['affiliation']);
  }


  // Save values to cookie.
  uchicago_cache_set_session_value('interests', $form_state['values']['interest-fieldset']);
  uchicago_cache_set_session_value('affiliation', $form_state['values']['affiliation-fieldset']);
  uchicago_cache_set_session_value('location', $form_state['values']['location']);

  //dpm($form_state['values']);

  // TODO Change message.
  if (isset($form_state['values'])) {
    drupal_set_message('Changes Saved');
  }

  // Tell the browser to close the modal.
  //$form_state['ajax_commands'][] = ctools_modal_command_dismiss();

  // Reload page in order to see changes.
  $form_state['ajax_commands'][] = ctools_ajax_command_reload();
}

/**
 * Increment user count by 1 and save back to taxonomy entity.
 */
function uchicago_cache_user_count_increment($tid) {
  $wrapper = entity_metadata_wrapper('taxonomy_term', taxonomy_term_load($tid));
  $user_count = $wrapper->field_user_count->value();
  $wrapper->field_user_count->set(++$user_count);
  $wrapper->save();
  //dpm($wrapper->field_user_count->value());
}

/**
 * Verify if term has been checked and is different from cached value.
 */
function uchicago_cache_user_count_check($term, $tid, $check_term = FALSE) {
  if ($term[$tid] == 1 && $term[$tid] !== $check_term[$tid]) {
    uchicago_cache_user_count_increment($tid);
  }
}

// CTOOLS MODAL FUNCTIONS.
/**
 * Implements hook_menu().
 */
function uchicago_cache_menu() {
  $items = array();

  $items['interests'] = array(
    'title' => 'UChicago Interests Form',
    'page callback' => 'uchicago_cache_page',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['uchicago_cache/%ctools_js'] = array(
    'page callback' => 'uchicago_cache_modal_form',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Helper function to make a link.
 */
function _uchicago_cache_make_link($link_text = '') {
  // Set a default value if no text in supplied.
  if (empty($link_text)) {
    $link_text = 'Customize Your Experience';
  }

  return '<div class="interests-modal-link">' . l($link_text, 'uchicago_cache/nojs', array('attributes' => array('class' => 'ctools-use-modal'))) . '</div>';
}

/**
 * An example page.
 */
function uchicago_cache_page() {
  return drupal_get_form('uchicago_cache_form');
}

/**
 * Ajax menu callback.
 */
function uchicago_cache_modal_form($ajax) {
  if ($ajax) {
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('UChicago Interests Modal Form'),
    );

    // Use ctools to generate ajax instructions for the browser to create
    // a form in a modal popup.
    $output = ctools_modal_form_wrapper('uchicago_cache_form', $form_state);

    // If the form has been submitted, there may be additional instructions
    // such as dismissing the modal popup.
    if (!empty($form_state['ajax_commands'])) {
      $output = $form_state['ajax_commands'];
    }

    // Return the ajax instructions to the browser via ajax_render().
    print ajax_render($output);
    drupal_exit();
  }
  else {
    return drupal_get_form('uchicago_cache_form');
  }
}

/**
 * Implements hook_block_info().
 */
function uchicago_cache_block_info() {
  $blocks['interest_form'] = array(
    // The name that will appear in the block list.
    'info' => t('Interests Form Modal Link'),
    // Default setting.
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}

function uchicago_cache_block_view($delta) {
  $block = array();

  switch ($delta) {
    case 'interest_form':
      // Load the modal library and add the modal javascript.
      ctools_include('ajax');
      ctools_include('modal');
      ctools_modal_add_js();
      $block['subject'] = t('Interests Form Modal');
      $block['content'] = _uchicago_cache_make_link();
      break;
  }
  return $block;
}
