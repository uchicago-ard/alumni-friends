<?php

/**
 * @file
 * Feeds parser class for FFFFOUND!
 */

/**
 * Class definition for UCCO Parser.
 *
 * Parses RSS feeds returned from UCCO.
 */
class UCCOFeedsParser extends FeedsParser {

  /**
   * Parse the extra mapping sources provided by this parser.
   *
   * @param FeedsFetcherResult $fetcher_result
   * @param FeedsSource $source
   *
   * @see FeedsParser::parse()
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $ucco_feed = $fetcher_result->getRaw();
    $result = new FeedsParserResult();

    if (!defined('LIBXML_VERSION') || (version_compare(phpversion(), '5.1.0', '<'))) {
      @$sxml = simplexml_load_string($ucco_feed, NULL);
    }
    else {
      @$sxml = simplexml_load_string($ucco_feed, NULL, LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NOCDATA);
    }

    // Got a malformed XML.
    if ($sxml === FALSE || is_null($sxml)) {
      throw new Exception(t('FeedsUCCOParser: Malformed XML source.'));
    }

    $result = $this->parseUCCO($sxml, $source, $fetcher_result);

    return $result;
  }

  /**
   * Add the extra mapping sources provided by this parser.
   */
  public function getMappingSources() {
    return parent::getMappingSources() + array(
      'feed_title' => array(
        'name' => t('Feed title'),
        'description' => t('The title of the pulled feed.'),
      ),
      'guid' => array(
        'name' => t('GUID'),
      ),
      'url' => array(
        'name' => t('Event URL'),
        'description' => t('The URL event page in UCCO.'),
      ),
      'title' => array(
        'name' => t('Title'),
        'description' => t('Title.'),
      ),
      'description' => array(
        'name' => t('Description'),
      ),
      'subhead' => array(
        'name' => t('Subheading'),
      ),
      'event_start' => array(
        'name' => t('Event start time'),
      ),
      'event_end' => array(
        'name' => t('Event end time'),
      ),
      'event_location_name' => array(
        'name' => t('Location Name'),
      ),
      'event_location_street1' => array(
        'name' => t('Location Street Address Line 1'),
      ),
      'event_location_street2' => array(
        'name' => t('Location Street Address Line 2'),
      ),
      'event_location_city' => array(
        'name' => t('Location City'),
      ),
      'event_location_state' => array(
        'name' => t('Location State'),
      ),
      'event_location_zip' => array(
        'name' => t('Location Zipcode'),
      ),
      'event_location_country' => array(
        'name' => t('Location Country'),
      ),
      'event_location_latitude' => array(
        'name' => t('Location Latitude'),
      ),
      'event_location_longitude' => array(
        'name' => t('Location Longitude'),
      ),
      'club_nid' => array(
        'name' => t('Host Club NID'),
      ),
      'keywords' => array(
        'name' => t('Interests Taxonomy'),
      ),
      'event_type' => array(
        'name' => t('Event Type Taxonomy'),
      ),
      'image' => array(
        'name' => t('Image URL'),
      ),
    );
  }

  /**
   * Parse UCCO feed
   *
   * @param SimpleXMLElement $sxml
   * @param FeedsFetcherResult $fetcher_result
   * @param FeedsSource $source
   */
  private function parseUCCO(SimpleXMLElement $sxml, FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $fetcher_result->title = $feed_title = (string) $sxml->title;
    $result = new FeedsParserResult();

    // Iterate over entries in feed.
    foreach ($sxml->xpath('//item') as $entry) {

      // Get nodes in uchicagoalumni: namespace for UCCO information.
      $ucco = $entry->children('http://www.uchicagoalumni.org/');

      /*
       * Put location data into location module array format.
       * geocoder_widget_parse_locationfield requires data in this format.
       */
      $location = array(
        'name' => (string) $ucco->event_location_name,
        'street' => (string) $ucco->event_location_street1,
        'additional' => (string) $ucco->event_location_street2,
        'city' => (string) $ucco->event_location_city,
        'provence' => (string) $ucco->event_location_state,
        'postal_code' => (string) $ucco->event_location_zip,
        'country' => (string) $ucco->event_location_country,
      );

      // Calculate GPS coordinates and save to location array.
      $address = geocoder_widget_parse_locationfield($location);
      $point = geocoder("google", $address);
      $location['latitude'] = $point->coords[1];
      $location['longitude'] = $point->coords[0];
      //dpm($location);

      /*
       * Use site domain name as an ID to lookup NID for entity reference.
       */
      $club = explode(".", $ucco->event_hosted_by_siteurl);
      $club_id = $club[1];
      // EFQ to get club node with the matching ID string.
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'club')
        ->propertyCondition('status', 1)
        ->fieldCondition('field_ucco_id', 'value', $club_id)
        ->range(0, 1);
      $club_node = $query->execute();
      $club_nid = key($club_node['node']);
      //dpm($club_nid);

      /*
       * Get path to event image out of html from UCCO feed.
       */
      $event_image = (string) $ucco->event_image;
      $imagehtml = new DOMDocument();
      $imagehtml->loadHTML($event_image);
      $xpath = new DOMXPath($imagehtml);
      $image_src = $xpath->evaluate("string(//img/@src)");
      //dpm($image_src);

      /*
       * Create array of TIDs from list of keywords in feed.
       */
      $keywords = explode("::", $ucco->tag_keyword);
      $interests_tid = array();
      $event_types_tid = array();

      // Check value of keywords from UCCO and assign TIDs.
      foreach ($keywords as $keyword) {
        // All incoming keywords to lowercase to prevent case matching errors.
        $keyword = strtolower($keyword);
        switch ($keyword) {
          // Interests.
          case 'alumni law society':
          case 'military affinity group':
            // Law, Policy & Society.
            $interests_tid[15];
            break;

          case 'arts alumni':
            // Arts & Humanities.
            $interests_tid[12];
            break;

          case 'chicago economics society':
            // Economics & Business.
            $interests_tid[13];
            break;

          case 'life sciences alumni group':
            // Science & Medicine.
            $interests_tid[16];
            break;

          // Event Types.
          case 'athletic':
          case 'sporting event':
            // Athletics.
            $event_types_tid[1];
            break;

          case 'book club':
            // Book Club.
            $event_types_tid[2];
            break;

          case 'career':
          case 'networking':
          case 'career month':
            // Business.
            $event_types_tid[3];
            break;

          case 'community service':
            // Community Service.
            $event_types_tid[4];
            break;

          case 'culture':
          case 'cultural':
            // Culture.
            $event_types_tid[5];
            break;

          case 'meeting':
          case 'panel':
          case 'reception':
            // Meeting.
            $event_types_tid[8];
            break;

          case 'social':
            // Social.
            $event_types_tid[9];
            break;

          //Unmatched event types.
          //case 'lecture':
          //case 'tour':
          //case 'young alumni':
          //case 'family friendly':
          //case 'free':
          //case 'outdoor':

        }
      }

      /*
       * REPLACED BY SWITCH STATEMENT ABOVE
       * Create array of TIDs from list of keywords in feed.
       */
      /*
      $keywords = explode("::", $ucco->tag_keyword);
      // Get top level interests and event type taxonomy terms.
      $interests = uc_interests_taxonomy_get_nested_tree($max_depth = 1);
      $interests_tid = array();
      //dpm($interests);
      $event_types = uc_events_taxonomy_get_nested_tree($max_depth = 1);
      $event_types_tid = array();
      //dpm($event_types);

      // Check value of keywords from UCCO against taxonomy term names.
      foreach ($keywords as $keyword) {
        $keyword = ucwords($keyword);
        $found = FALSE;
        foreach($interests as $tid => $term) {
          //dpm($term);
          if ($term->name == $keyword) {
            $interests_tid[] = $tid;
            $found = TRUE;
            break;
          }
        }
        // If a keyword was matched in $interests, skip $event_types.
        if ($found == FALSE) {
          foreach($event_types as $tid => $term) {
            //dpm($term);
            if ($term->name == $keyword) {
              $event_types_tid[] = $tid;
              break;
            }
          }
        }
      }
      */


      //dpm($event_types_tid);
      //dpm($interests_tid);

      $item = array(
        'feed_title' => $feed_title,
        'guid' => (string) $entry->guid,
        'url' => (string) $entry->link,
        'title' => (string) $entry->title,
        'subhead' => (string) $ucco->event_subhead,
        'description' => html_entity_decode((string) $entry->description),
        'event_start' => (string) $ucco->event_start,
        'event_end' => (string) $ucco->event_end,
        'event_location_name' => (string) $location['name'],
        'event_location_street1' => (string) $location['street'],
        'event_location_street2' => (string) $location['additional'],
        'event_location_city' => (string) $location['city'],
        'event_location_state' => (string) $location['provence'],
        'event_location_zip' => (string) $location['postal_code'],
        'event_location_country' => (string) $location['country'],
        'event_location_latitude' => (string) $location['latitude'],
        'event_location_longitude' => (string) $location['longitude'],
        'image' => (string) $image_src,
        'keywords' => $interests_tid,
        'event_type' => $event_types_tid,
        'club_nid' => $club_nid,
      );

      // Populate the FeedsFetcherResult object with the parsed results.
      $result->items[] = $item;
      //dpm($item);
    }

    return $result;
  }

}
