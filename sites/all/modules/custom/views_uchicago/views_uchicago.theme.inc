<?php

/**
 * Preprocess function to build the filter nav block
 */

// TODO CLEAN UP THIS FUNCTION
function template_preprocess_views_uchicago_filter_nav(&$vars) {
  //template_preprocess_views_view_list($vars);

  // Extract useful data from view object.
  $view = $vars['view'];
  $handler = $view->style_plugin;
  //$fields = $view->field;
  //$result = $view->result;
  //$rows = &$vars['rows'];
  $selected_arg = $vars['options']['url_args'] - 1;
  $total_args = $vars['options']['url_args_total'] - 1;
  $args = &$vars['view']->args;
  // Path field specified in views UI
  $path_field = $handler->options['path'];
  $base_path = base_path();
  $current_path = request_path();

  //set undefined args to 'all' and remove args from $current_path
  for ($i = 0; $i <= $total_args; $i++) {
    if (!isset($args[$i])) {
      $args[$i] = 'all';
    }
    $current_path = str_replace('/' . $args[$i],'',$current_path);
  }

  //set new variable to prevent further changes to $vars
  $new_args = $args;

  foreach ($handler->rendered_fields as $index => $row) {
    $path = strtolower($row[$path_field]);
    $new_args[$selected_arg] = preg_replace("![^a-z0-9]+!i", "-", $path);
    $vars['url_paths'][$index] =  $base_path . $current_path . "/";
    for ($i = 0; $i <= $total_args; $i++) {
      $vars['url_paths'][$index] .= $new_args[$i] . "/";
    }
  }

  //set selected arg value in $new_args to 'all' for use in reset link
  $new_args[$selected_arg] = 'all';
  $vars['url_reset_path'] = $base_path . $current_path . "/";
  for ($i = 0; $i <= $total_args; $i++) {
    $vars['url_reset_path'] .= $new_args[$i] . "/";
  }

}
