<?php

/**
 * @file
 */

/**
 * Style plugin.
 */
class views_uchicago_plugin_style_filter_nav extends views_plugin_style {

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['label'] = array('default' => '', 'translatable' => TRUE);
    $options['path'] = array('default' => '');
    $options['url_args'] = array('default' => '');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $handlers = $this->display->handler->get_handlers('field');
    if (empty($handlers)) {
      $form['error_markup'] = array(
        '#markup' => t('You need at least one field before you can configure your jump menu settings'),
        '#prefix' => '<div class="error messages">',
        '#suffix' => '</div>',
      );
      return;
    }

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Selector label'),
      '#default_value' => $this->options['label'],
      '#description' => t('The text that will appear as the the label of the selector element. If blank no label tag will be used.'),
    );

    foreach ($handlers as $id => $handler) {
      $options[$id] = $handler->ui_name();
    }

    $form['path'] = array(
      '#type' => 'select',
      '#title' => t('Path field'),
      '#options' => $options,
      '#default_value' => $this->options['path'],
      '#description' => t('Select one field that will represent the value of the URL path. The value of this field will be the URL paramater used in the URL string.  This field should match the criteria of the contextual filter.'),
    );


    $form['url_args'] = array(
      '#type' => 'textfield',
      '#title' => t('URL Argument Position'),
      '#default_value' => $this->options['url_args'],
      '#description' => t('Specifiy the numerical value representing the position in the URL path this list will control.'),
    );

    $form['url_args_total'] = array(
      '#type' => 'textfield',
      '#title' => t('Total Number of URL Argumements'),
      '#default_value' => $this->options['url_args_total'],
      '#description' => t('Specifiy the total number of Contextual Filter variables in use'),
    );

  }

}
