<?php
/**
 * @file views-uchicago-filter-nav.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

$selected_arg = $options['url_args'] -1;
$title = $view->get_title();
?>
<fieldset>
<?php if (!empty($title)) : ?>
  <legend>
    <span class="fieldset-legend">
      <a class="fieldset-title" href="#">
        <?php print $title; ?>
      </a>
    </span>
  </legend>
<?php endif; ?>
<?php if ($view->args[$selected_arg] != "all") : ?>
  <p>
    <a href="<?php print $url_reset_path;?>">Remove Filter</a>
  </p>
<?php endif; ?>
<ul class="filter-nav-container">
  <?php
  $count = 0;
  foreach ($rows as $row):
    ?>
    <li class="filter-nav">
      <a href="<?php print $url_paths[$count];?>">
        <?php print $row;?>
      </a>
    </li>
    <?php
    $count++;
  endforeach;
  ?>
</ul>
</fieldset>
