<?php

/**
 * @file
 * Creates a GPS filter nav pane.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('GPS Filter Pane'),
  'description' => t('A GPS filter for views contextual filters.'),
  'category' => t('UChicago'),
  'edit form' => 'views_uchicago_gps_filter_pane_edit_form',
  'render callback' => 'views_uchicago_gps_filter_pane_render',
  'defaults' => array(
    'text' => '',
    'argument_index' => '',
  ),
);

/**
 * An edit form for the pane's settings.
 */
function views_uchicago_gps_filter_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['text'] = array(
    '#type' => 'textfield',
    '#title' => t('Fieldset Header'),
    '#description' => t('The title to display on the fieldset.'),
    '#default_value' => $conf['text'],
  );
  $form['argument_index'] = array(
    '#type' => 'textfield',
    '#title' => t('Argument Index'),
    '#description' => t('An integer representing the position in path of the gps argument.'),
    '#default_value' => $conf['argument_index'],
  );

  return $form;
}

/**
 * Submit function, note anything in the formstate[conf] automatically gets saved
 */
function views_uchicago_gps_filter_pane_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function views_uchicago_gps_filter_pane_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $block->title = '';
  $block->content = drupal_get_form('uchicago_gps_filter_pane_form', $conf, $args);
  return $block;
}

/**
 * Makes the GPS Filter form.
 *
 * @param array $conf
 *
 * @param array $args
 */
function uchicago_gps_filter_pane_form($form, &$form_state, $conf, $args) {
  // Get cached form from previous submit if it exists.
  $cached_form_state = uchicago_cache_get_session_value('gps_form');
  // If cached form doesn't exist, get location values from interest form or ip lookup.
  $location = isset($cached_form_state['location']) ? $cached_form_state['location'] : uchicago_get_cached_loc();

  // Push variables to form_state so they can be passed on when submitted.
  $argument_index = (integer) $conf['argument_index'];
  $form_state['argument_index'] = $argument_index;

  $args_count = count($args);
  // If args less than the value of argument_index make sure we generate enough placeholder args.
  $total_args = max(array($argument_index, $args_count));
  $form_state['total_args'] = $total_args;

  // Set undefined args to placeholder 'all'.
  for ($i = 0; $i < $total_args; $i++) {
    if (!isset($args[$i])) {
      $args[$i] = 'all';
    }
  }
  $form_state['args'] = $args;

  // Find basepath and remove args from it.
  // E.g. 'events/all/harper-lecture' >> 'events'
  $basepath = explode('/', request_path());
  for ($i = $args_count; $i > 0; $i--) {
    array_pop($basepath);
  }
  $basepath = implode('/', $basepath);
  $form_state['basepath'] = $basepath;

  require_once(DRUPAL_ROOT . '/includes/locale.inc');
  $country_list = country_get_list();

  $form['location'] = array(
    '#type' => 'fieldset',
    '#title' => $conf['text'],
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['location']['country_code'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => $country_list,
    '#default_value' => isset($location['country_code']) ? $location['country_code'] : 'US',
  );
  $form['location']['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#description' => t('Enter any combination of address, city, state/provence, or postal code.'),
    '#default_value' => isset($location['address']) ? $location['address'] : (isset($location['postal_code']) ? $location['postal_code'] : ''),
    '#input_group' => TRUE,
    '#field_suffix' => '<i class="fa fa-globe"></i>',
  );
  $form['location']['range'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance'),
    '#description' => t('Specify a search radius in miles.'),
    '#default_value' => isset($location['range']) ? $location['range'] : '100',
    '#input_group' => TRUE,
    '#field_suffix' => t('Miles'),
  );

  $form['location']['actions'] = array('#type' => 'actions');
  $form['location']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  // If current page has a GPS filter applied, provide a reset link.
  if ($args[$argument_index - 1] !== 'all') {
    $args[$argument_index - 1] = 'all';
    $form['location']['actions']['remove'] = array(
      '#markup' => l(t('Remove Location Filter'), $basepath . '/' . implode('/', $args)),
    );
  }

  return $form;
}

function uchicago_gps_filter_pane_form_validate($form, &$form_state) {
}

/**
 * Generate GPS coordinates and redirect to new filtered page.
 */
function uchicago_gps_filter_pane_form_submit($form, &$form_state) {
  // Lookup GPS coordinates by address and country.
  $address = $form_state['values']['location']['address'] . " " . $form_state['values']['location']['country_code'];
  $point = geocoder("google", $address);
  $form_state['values']['location']['latitude'] = $point->coords[1];
  $form_state['values']['location']['longitude'] = $point->coords[0];
  // Add GPS coordinates string to args in correct position.
  $args = $form_state['args'];
  $args[($form_state['argument_index'] - 1)] = $point->coords[1] . ',' . $point->coords[0] . '_' . $form_state['values']['location']['range'];
  // Assemble path from args and basepath.
  $path = implode('/', $args);
  // Redirect to new filtered page.
  $form_state['redirect'] = $form_state['basepath'] . '/' . $path;
  // Cache form_state in cookie so it can be used to persit form values on redirect.
  uchicago_cache_set_session_value('gps_form', $form_state['values']);
}
