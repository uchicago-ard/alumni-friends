<?php

/**
 * Implements hook_views_plugins().
 */
function views_uchicago_views_plugins(){
  $plugins = array();

  $plugins['style']['uchicago_filter_nav'] = array(
    'title' => t('Contextual Filter Navigation'),
    'help' => t('Contextual Filter Navigation object.'),
    'handler' => 'views_uchicago_plugin_style_filter_nav',
    'theme' => 'views_uchicago_filter_nav',
    'uses options' => TRUE,
    'uses row plugin' => TRUE,
    'uses fields' => TRUE,
    'uses grouping' => FALSE,
    'type' => 'normal',
    'theme file' => 'views_uchicago.theme.inc',
  );

  return $plugins;
}

