<?php

/**
 * @file
 * UChicago Right Top Layout
 */
?>

<div class="panel-layout uchicago-content-left uchicago-content-top clearfix <?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print 'id="' . $css_id . '"'; } ?>>
  <figure class="uchicago-section uchicago-section-banner clearfix">
    <?php print $content['banner']; ?>
  </figure>
  <div class="container clearfix">
    <section class="uchicago-section uchicago-section-main shadow">
      <?php print $content['main']; ?>
    </section>
    <section class="uchicago-section uchicago-section-aside">
      <?php print $content['caption']; ?>

      <?php if ($content['aside1'] && $content['aside2']): ?>
        <div class="clearfix uchicago-section-container">
          <div class="uchicago-section-aside-left">
            <?php print $content['aside1']; ?>
          </div>
          <div class="uchicago-section-aside-right">
            <?php print $content['aside2']; ?>
          </div>
        </div>
      <?php endif ?>

      <?php if ($content['aside3']): ?>
        <div class="uchicago-section-aside-full uchicago-section-container clearfix">
          <?php print $content['aside3']; ?>
        </div>
      <?php endif ?>

      <?php if ($content['aside4'] && $content['aside5']): ?>
        <div class="clearfix uchicago-section-container">
          <div class="uchicago-section-aside-left">
            <?php print $content['aside4']; ?>
          </div>
          <div class="uchicago-section-aside-right">
            <?php print $content['aside5']; ?>
          </div>
        </div>
      <?php endif ?>

    </section>
  </div>
</div>
