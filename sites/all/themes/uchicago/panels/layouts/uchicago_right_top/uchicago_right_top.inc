<?php

$plugin = array(
  'title' => t('UChicago Right Top'),
  'icon' => 'uchicago-right-top.png',
  'category' => t('UChicago'),
  'theme' => 'uchicago_right_top',
  'regions' => array(
    'banner' => t('Banner Image'),
    'main' => t('Main Text Area'),
    'caption' => t('Image Caption'),
    'aside1' => t('Secondary Content Left'),
    'aside2' => t('Secondary Content Right'),
    'aside3' => t('Secondary Content Full Width'),
    'aside4' => t('Secondary Content Left 2'),
    'aside5' => t('Secondary Content Right 2'),
  ),
);
