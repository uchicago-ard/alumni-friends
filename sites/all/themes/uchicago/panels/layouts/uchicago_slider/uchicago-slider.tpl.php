<?php

/**
 * @file
 * UChicago Right Top Layout
 */

?>

<div class="panel-layout uchicago-slider <?php if (isset($page_layout)) { print $page_layout; } ?> <?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print 'id="' . $css_id . '"'; } ?>>
  <figure class="uchicago-section uchicago-section-banner">
    <?php print $content['banner']; ?>
  </figure>
  <a class="uchicago-section uchicago-section-main" href="<?php print $node_path; ?>">
    <div class="title-text <?php if (isset($title_style)) { print 'pane-' . $title_style; } ?>">
      <?php print $content['title']; ?>
    </div>
    <div class="teaser-text">
      <?php print $content['teaser']; ?>
    </div>
  </a>
</div>

