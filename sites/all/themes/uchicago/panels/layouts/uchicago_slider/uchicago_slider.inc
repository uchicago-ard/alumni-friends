<?php

$plugin = array(
  'title' => t('UChicago Slider'),
  'icon' => 'uchicago-slider.png',
  'category' => t('UChicago'),
  'theme' => 'uchicago_slider',
  'regions' => array(
    'banner' => t('Banner Image'),
    'title' => t('Node Title'),
    'teaser' => t('Teaser Text'),
  ),
);
