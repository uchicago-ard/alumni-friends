<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="no-js ie6 oldie"
      lang="<?php print $language->language; ?>"><![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7 oldie"
      lang="<?php print $language->language; ?>"><![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8 oldie"
      lang="<?php print $language->language; ?>"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php print $language->language; ?>"> <!--<![endif]-->

<?php print $mothership_poorthemers_helper; ?>

<?php if (theme_get_setting('mothership_html5')){ ?>
<head>
  <?php }else{ ?>
<head profile="<?php print $grddl_profile; ?>">
  <?php } ?>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $appletouchicon; ?>

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <?php if (theme_get_setting('mothership_viewport')) { ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php } ?>

  <style type="text/css" media="screen">
    @import url("<?php print $base_path.$path; ?>/css/style.css");
  </style>

  <?php if (theme_get_setting('mothership_html5')) { ?>
    <!--[if lt IE 9]>
    <script
      src="<?php print drupal_get_path('theme', 'mothership'); ?>/js/html5.js"></script>
    <![endif]-->
  <?php } ?>
  <?php if (isset($selectivizr)) { ?>
    <?php print $selectivizr; ?>
  <?php } ?>

</head>

<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<header id="header" role="banner">
  <div class="logo-title">
    <div class="container">
      <div class="title-slogan">
        <a href="<?php print $front_page; ?>" rel="home">
          <h2 class="site-title"><?php print $site_name; ?></h2>
          <h5 class="site-slogan"><?php if ($site_slogan):
              print $site_slogan;
            endif; ?> &nbsp
          </h5>
        </a>
      </div>
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home" class="logo">
          <?php print $site_name; ?>
        </a>
      <?php endif; ?>
    </div>
  </div>
</header>
<div id="messages">
  <?php print $messages; ?>
</div>
<main id="main" role="main">
  <div class="container">
    <?php if ($help): ?>
      <div id="help">
        <?php print $help; ?>
      </div>
    <?php endif; ?>
    <?php print $content; ?>
  </div>
</main>

<footer id="footer" role="contentinfo" class="clearfix">
  <div class="container">
    <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home" class="logo">
      <?php print $site_name; ?>
    </a>
  </div>
</footer>


</body>
</html>


