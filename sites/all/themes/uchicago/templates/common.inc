<?php
/**
 * @file
 * common.inc
 *
 * Contains common functionality for the entire base theme.
 */

/**
 * Helper function for including theme files.
 *
 * @param string $theme
 *   Name of the theme to use for base path.
 * @param string $path
 *   Path relative to $theme.
 */
function uchicago_include($theme, $path) {
  static $themes = array();
  if (!isset($themes[$theme])) {
    $themes[$theme] = drupal_get_path('theme', $theme);
  }
  if ($themes[$theme] && ($file = DRUPAL_ROOT . '/' . $themes[$theme] . '/' . $path) && file_exists($file)) {
    include_once $file;
  }
}

/**
 * Theme an Icon.
 *
 * @param string $name
 *   The icon name, minus the "fa-" prefix.
 *
 * @return string
 *   The icon HTML markup.
 */
function _uchicago_icon($name) {
  $output = '';
  // Attempt to use the Icon API module, if enabled and it generates output.
  if (module_exists('icon')) {
    $output = theme('icon', array('bundle' => 'fa', 'icon' => 'fa-' . $name));
  }
  if (empty($output)) {
    // Mimic the Icon API markup.
    $attributes = array(
      'class' => array('icon', 'fa', 'fa-' . $name),
      'aria-hidden' => 'true',
    );
    $output = '<i' . drupal_attributes($attributes) . '></i>';
  }
  return $output;
}

/**
 * Colorize buttons based on the text value.
 *
 * @param string $text
 *   Button text to search against.
 *
 * @return string
 *   The specific button class to use or FALSE if not matched.
 */
function _uchicago_colorize_button($text) {
  // Text values containing these specific strings, which are matched first.
  $specific_strings = array(
    'btn-default' => array(
      t('Save as draft'),
      t('Unpublish'),
    ),
    'btn-primary' => array(
      t('Download feature'),
      t('Publish'),
    ),
    'btn-success' => array(
      t('Add effect'),
      t('Add and configure'),
    ),
    'btn-info' => array(
      t('Save and add'),
      t('Add another item'),
      t('Update style'),
    ),
  );
  // Text values containing these generic strings, which are matches last.
  $generic_strings = array(
    'btn-primary' => array(
      t('Save'),
      t('Confirm'),
      t('Submit'),
      t('Search'),
    ),
    'btn-success' => array(
      t('Add'),
      t('Create'),
      t('Write'),
      t('Finish'),
    ),
    'btn-warning' => array(
      t('Export'),
      t('Import'),
      t('Restore'),
      t('Rebuild'),
      t('Revert'),
    ),
    'btn-info' => array(
      t('Apply'),
      t('Update'),
    ),
    'btn-danger' => array(
      t('Delete'),
      t('Remove'),
    ),
    'btn-caution' => array(
      t('Cancel'),
    ),
  );
  // Specific matching first.
  foreach ($specific_strings as $class => $strings) {
    foreach ($strings as $string) {
      if (strpos(drupal_strtolower($text), drupal_strtolower($string)) !== FALSE) {
        return $class;
      }
    }
  }
  // Generic matching last.
  foreach ($generic_strings as $class => $strings) {
    foreach ($strings as $string) {
      if (strpos(drupal_strtolower($text), drupal_strtolower($string)) !== FALSE) {
        return $class;
      }
    }
  }
  return FALSE;
}

/**
 * Iconize buttons based on the text value.
 *
 * @param string $text
 *   Button text to search against.
 *
 * @return string
 *   The icon markup or FALSE if not matched.
 */
function _uchicago_iconize_button($text) {
  // Text values containing these specific strings, which are matched first.
  $specific_strings = array(
    'archive' => array(
      t('Unpublish'),
    ),
  );
  // Text values containing these generic strings, which are matches last.
  $generic_strings = array(
    'cog' => array(
      t('Manage'),
      t('Configure'),
    ),
    'download' => array(
      t('Download'),
      t('Import'),
    ),
    'pencil' => array(
      t('Edit'),
    ),
    'plus' => array(
      t('Add'),
      t('Write'),
    ),
    'trash-o' => array(
      t('Delete'),
      t('Remove'),
    ),
    'upload' => array(
      t('Upload'),
      t('Export'),
    ),
    'floppy-o' => array(
      t('Save'),
    ),
    'exclamation-triangle' => array(
      t('Revert'),
    ),
    'undo' => array(
      t('Undo'),
      t('Back'),
    ),
    'minus-circle' => array(
      t('Cancel'),
    ),
    'key' => array(
      t('Log in'),
    ),
    'thumbs-o-up' => array(
      t('Publish'),
    ),
  );
  // Specific matching first.
  foreach ($specific_strings as $icon => $strings) {
    foreach ($strings as $string) {
      if (strpos(drupal_strtolower($text), drupal_strtolower($string)) !== FALSE) {
        return _uchicago_icon($icon);
      }
    }
  }
  // Generic matching last.
  foreach ($generic_strings as $icon => $strings) {
    foreach ($strings as $string) {
      if (strpos(drupal_strtolower($text), drupal_strtolower($string)) !== FALSE) {
        return _uchicago_icon($icon);
      }
    }
  }
  return FALSE;
}
