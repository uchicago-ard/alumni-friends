<?php
/**
 * @file
 * button.func.php
 */

/**
 * Overrides theme_select().
 */
function uchicago_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'size'));

  if(!theme_get_setting('mothership_classes_form_input')){
    _form_set_class($element, array('form-select'));
  }

  $output = '';
  if ($element['#multiple'] !== TRUE) {
    $output .= '<span class="select">';
  }
  $output .= '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select>';
  if ($element['#multiple'] !== TRUE) {
    $output .= '</span>';
  }

  return $output;
}
