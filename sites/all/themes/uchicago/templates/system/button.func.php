<?php
/**
 * @file
 * button.func.php
 */

/**
 * Overrides theme_button().
 */
function uchicago_button($variables) {
  $element = $variables['element'];
  $label = $element['#value'];
  element_set_attributes($element, array('id', 'name', 'value', 'type'));

  // If a button type class isn't present then add in default.
  $button_classes = array(
    'btn-default',
    'btn-primary',
    'btn-success',
    'btn-info',
    'btn-warning',
    'btn-danger',
    'btn-link',
  );
  $class_intersection = array_intersect($button_classes, $element['#attributes']['class']);
  if (empty($class_intersection)) {
    $element['#attributes']['class'][] = 'btn-default';
  }

  // Add in the button type class.
  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];

  $output = '';

  $output .= '<button' . drupal_attributes($element['#attributes']) . '>';

  if (isset($element['#icon'])) {
    $output .= $element['#icon'];
  }

  // This line break adds inherent margin between multiple buttons.
  $output .= $label . "</button>\n";

  return $output;
}
