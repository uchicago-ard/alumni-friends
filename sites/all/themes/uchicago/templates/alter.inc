<?php
/**
 * @file
 * alter.inc
 *
 * Contains various implementations of hook_*_alter().
 */

/**
 * Implements hook_css_alter().
 */
function uchicago_css_alter(&$css) {
  $theme_path = drupal_get_path('theme', 'uchicago');

  if (isset($css['profiles/panopoly/modules/contrib/panels/css/panels_dnd.css'])) {
    $css['profiles/panopoly/modules/contrib/panels/css/panels_dnd.css']['data'] = $theme_path . '/css/panels_dnd.css';
  }
  if (isset($css['sites/all/libraries/chosen/chosen.css'])) {
    unset($css['sites/all/libraries/chosen/chosen.css']);
  }
  //dpm($css);
}
// */

/**
 * Implements hook_form_alter().
 */
/* -- Delete this line if you want to use this function
function uchicago_form_alter(array &$form, array &$form_state = array(), $form_id = NULL) {
  //if ($form_id == '') {
  //....
  //}
}
// */


/**
 * Implements hook_js_alter().
 */
function uchicago_js_alter(&$js) {
  $theme_path = drupal_get_path('theme', 'uchicago');

  // Add or replace JavaScript files when matching paths are detected.
  // Replacement files must begin with '_', like '_node.js'.
  $files = file_scan_directory($theme_path . '/js', '/\.js$/');
  foreach ($files as $file) {
    $path = str_replace($theme_path . '/js/', '', $file->uri);
    // Detect if this is a replacement file.
    $replace = FALSE;
    if (preg_match('/^[_]/', $file->filename)) {
      $replace = TRUE;
      $path = dirname($path) . '/' . preg_replace('/^[_]/', '', $file->filename);
    }
    $matches = array();
    if (preg_match('/^modules\/([^\/]*)/', $path, $matches)) {
      if (!module_exists($matches[1])) {
        continue;
      }
      else {
        $path = str_replace('modules/' . $matches[1], drupal_get_path('module', $matches[1]), $path);
      }
    }
    // Path should always exist to either add or replace JavaScript file.
    if (!empty($js[$path])) {
      // Replace file.
      if ($replace) {
        $js[$file->uri] = $js[$path];
        $js[$file->uri]['data'] = $file->uri;
        unset($js[$path]);
      }
      // Add file.
      else {
        $js[$file->uri] = drupal_js_defaults($file->uri);
        $js[$file->uri]['group'] = JS_THEME;
      }
    }
  }

  // Add non-replacement JavaScript files
  $theme_script = $theme_path . '/js/script.js';
  $js[$theme_script] = drupal_js_defaults($theme_script);
  $js[$theme_script]['group'] = JS_THEME;
  $js[$theme_script]['scope'] = 'footer';

  $theme_plugins = $theme_path . '/js/plugins.js';
  $js[$theme_plugins] = drupal_js_defaults($theme_plugins);
  $js[$theme_plugins]['group'] = JS_LIBRARY;
  $js[$theme_plugins]['scope'] = 'header';
}

/**
 * Include #pre_render callbacks for elements.
 */
//uchicago_include('uchicago', 'theme/pre-render.inc');

/**
 * Include #process callbacks for elements.
 */
//uchicago_include('uchicago', 'theme/process.inc');
