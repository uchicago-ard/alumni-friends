<?php if (theme_get_setting('mothership_poorthemers_helper')) { ?>
  <!-- .tpl: fieldable-panels-pane.tpl.php -->
  <!--  path: <?php print (__FILE__) ?> -->
  <!--  Suggestions: -->
  <?php
  foreach ($theme_hook_suggestions as $key => $value) {
    print '<!--' . $value . '  --> ';
  }
  ?>
<?php } ?>
<?php print render($title_suffix); ?>
<?php print $fields; ?>
<?php if (theme_get_setting('mothership_poorthemers_helper')) { ?>
  <!-- / tpl: fieldable-panels-pane.tpl.php -->
<?php } ?>
