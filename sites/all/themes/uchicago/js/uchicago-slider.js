(function ($) {

  $(document).ready(function () {

    var $window = $(window);
    var $front_page_rotator = $('#front-page-rotator');
    $front_page_rotator.addClass('animate');
    //var $slideshow_image = $front_page_rotator.find("figure.uchicago-section-banner:first img.borealis");

    // Sub Features
    var subfeature_options = {
      autoPlay: true,
      nextButton: false,
      prevButton: false,
      preloader: false,
      hideFramesUntilPreloaded: false,
      pauseOnHover: true,
      animateStartingFrameIn: true,
      transitionThreshold: 250,
      reverseAnimationsWhenNavigatingBackwards: false,
      autoPlayDelay: 6000,
      swipeNavigation: true,
      swipeEvents: {left: 'next', right: 'prev', up: false, down: false}
    };
    var sequence = $front_page_rotator.sequence(subfeature_options).data('sequence');

    if (typeof sequence != "undefined") {

      sequence.afterLoaded = function () {

        /*
        $('#featurenav').fadeIn(100);
        $('#featurenav li:nth-child(' + (sequence.settings.startingFrameID) + ') button').addClass('active');
        if (!sequence.transitionsSupported) {
          $("#features").animate({"opacity": "1"}, 1000);
        }
        */

      };

      sequence.afterNextFrameAnimatesIn = function () {
        this.currentFrame.removeClass("animate-out");
      };

      sequence.beforeNextFrameAnimatesIn = function () {

        $next_frame = sequence.nextFrame;
        sliderResize($next_frame);

        /*
        $('ul#features_container li:nth-child(' + (sequence.nextFrameID) + ') img').each(function () {
          var $self = $(this);
          if ($self.attr('data-original')) {
            this.src = $self.attr('data-original');
          }
        });
        $('#featurenav li:not(:nth-child(' + (sequence.nextFrameID) + ')) button').removeClass('active');
        $('#featurenav li:nth-child(' + (sequence.nextFrameID) + ') button').addClass('active');
         */

      };
    }

    /*
    $('#featurenav li').bind('click focusin', function () {
      if (!sequence.active) {
        $(this).children('button').removeClass('active').children('button').addClass('active');
        sequence.nextFrameID = $(this).index() + 1;
        sequence.goTo(sequence.nextFrameID);
      }
    });
    */

    //resize background to match window
    function sliderResize(frame) {
      var container = frame.find(".uchicago-slider");
      var container_height = container.height();
      console.log(container_height);
      $front_page_rotator.css({"height":container_height});
    };

    $window.on('debouncedresize', function () {
      $current_frame = $front_page_rotator.find(".animate-in");
      sliderResize($current_frame);
    });

  });

})(jQuery);

