(function ($) {

    $window = $(window);
    $body = $("body");
    $page = $('#page-page');
    $banner = $page.children($('.uchicago-section-banner'));
    $main_content = $page.find($('.uchicago-section-main'));

    function mobileMenuToggle() {
        $body.toggleClass('mobile-menu-active');
    }

    function adjustHeight() {
        if($window.width() > 999) {
            var banner_height = $banner.height();
            if ($page.hasClass('uchicago-content-bottom')) {
                $main_content.css({"margin-top": -(banner_height * 0.55)});
            } else if($page.hasClass('uchicago-content-top')) {
                $main_content.css({"margin-top": -(banner_height * 0.9)});
            }
        } else {
            $main_content.css({"margin-top": ''});
        }
    }

    $(document).ready(function () {
        var $mobileMenu = $(".mobile-menu-toggle");
        $mobileMenu.click( function() {
            mobileMenuToggle();
        })
    });

    $window.on('load', function () {
        adjustHeight();
    });

    $window.on('debouncedresize', function (event) {
        adjustHeight();
    });

})(jQuery);
