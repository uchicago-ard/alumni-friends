<?php
/**
 * Include common functions used through out theme.
 */
include_once dirname(__FILE__) . '/templates/common.inc';

/**
 * Implements hook_theme().
 *
 * Register theme hook implementations.
 *
 * The implementations declared by this hook have two purposes: either they
 * specify how a particular render array is to be rendered as HTML (this is
 * usually the case if the theme function is assigned to the render array's
 * #theme property), or they return the HTML that should be returned by an
 * invocation of theme().
 *
 * @see _uchicago_theme()
 */
function uchicago_theme(&$existing, $type, $theme, $path) {
  uchicago_include($theme, 'templates/registry.inc');
  return _uchicago_theme($existing, $type, $theme, $path);
}

/**
 * Declare various hook_*_alter() hooks.
 *
 * hook_*_alter() implementations must live (via include) inside this file so
 * they are properly detected when drupal_alter() is invoked.
 */
uchicago_include('uchicago', 'templates/alter.inc');


/**
 * Implements hook_preprocess_html()
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function uchicago_preprocess_html(&$vars) {
  global $base_url;
  $path = drupal_get_path('theme', 'uchicago');

  //Change Mothership default icons
  $appletouchicon = '<link rel="apple-touch-icon" sizes="152x152" href="' . $base_url .'/'. $path . '/icons/apple-touch-icon-152x152.png">'. "\n";
  $appletouchicon .= '<link rel="apple-touch-icon" sizes="144x144" href="' . $base_url .'/'. $path . '/icons/apple-touch-icon-144x144.png">'. "\n";
  $appletouchicon .= '<link rel="apple-touch-icon" sizes="120x120" href="' . $base_url .'/'. $path . '/icons/apple-touch-icon-120x120.png">'. "\n";
  $appletouchicon .= '<link rel="apple-touch-icon" sizes="114x114" href="' . $base_url .'/'. $path . '/icons/apple-touch-icon-114x114.png">'. "\n";
  $appletouchicon .= '<link rel="apple-touch-icon" sizes="76x76" href="' . $base_url .'/'.  $path . '/icons/apple-touch-icon-76x76.png">' . "\n";
  $appletouchicon .= '<link rel="apple-touch-icon" sizes="72x72" href="' . $base_url .'/'.  $path . '/icons/apple-touch-icon-72x72.png">' . "\n";
  $appletouchicon .= '<link rel="apple-touch-icon" sizes="57x57" href="' . $base_url .'/'.  $path . '/icons/apple-touch-icon-57x57.png">' . "\n";
  $appletouchicon .=  '<link rel="apple-touch-icon" href="' . $base_url .'/'.  $path . '/icons/apple-touch-icon.png">' . "\n";
  //$appletouchicon .=  '<link rel="apple-touch-startup-image" href="' . $base_url .'/'.  $path . '/icons/apple-startup.png">' . "\n";

  $vars['appletouchicon'] = $appletouchicon;
}
//*/

/**
 * Override or insert variables into the page template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function uchicago_preprocess_page(&$vars,$hook) {
  // Format and add main menu to theme.
  $vars['main_menu'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
  $vars['main_menu']['#theme_wrappers'] = array('menu_tree__primary');

  // Add a copyright message.
  $current_year = date('Y');
  $vars['copyright'] = "Copyright " . $current_year . " University of Chicago, Alumni Relations and Development<br />5235 S. Harper Ct. Chicago, IL, 60615";
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function uchicago_preprocess_region(&$vars,$hook) {
  //  kpr($vars['content']);
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function uchicago_preprocess_block(&$vars, $hook) {
  //  kpr($vars['content']);

  //lets look for unique block in a region $region-$blockcreator-$delta
   $block =
   $vars['elements']['#block']->region .'-'.
   $vars['elements']['#block']->module .'-'.
   $vars['elements']['#block']->delta;

  // print $block .' ';
   switch ($block) {
     case 'header-menu_block-2':
       $vars['classes_array'][] = '';
       break;
     case 'sidebar-system-navigation':
       $vars['classes_array'][] = '';
       break;
    default:

    break;

   }


  switch ($vars['elements']['#block']->region) {
    case 'header':
      $vars['classes_array'][] = '';
      break;
    case 'sidebar':
      $vars['classes_array'][] = '';
      $vars['classes_array'][] = '';
      break;
    default:

      break;
  }

}
// */

/**
 * Override or insert variables into the node template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function uchicago_preprocess_node(&$vars,$hook) {
  if ($vars['view_mode'] == 'search_result') {
    $node = $vars['node'];

    // Load slider JS.
    $theme_path = path_to_theme();
    drupal_add_js(libraries_get_path('sequence') . '/scripts/jquery.sequence.js');
    drupal_add_js($theme_path . '/js/uchicago-slider.js');

    // Get NIDs and path to nodes.
    $nid = $node->nid;
    $path = url("node/" . $nid);

    // Get custom page layout settings.
    $page_layout = $node->panelizer['page_manager']->display->layout;

    // Reformat page layout variable for use as CSS class.
    $page_layout = str_replace("_bottom", "", $page_layout);
    $page_layout = str_replace("_top", "", $page_layout);
    $page_layout = str_replace('_', '-', $page_layout);

    // Get custom title panel color settings and reformat for use as CSS class.
    $node_content = $node->panelizer['page_manager']->display->content;
    foreach ($node_content as $key => $value) {
      if ($value->type == "node_title") {
        $title_id = $key;
        if (!empty($node->panelizer['page_manager']->display->content[$title_id]->style)) {
          $title_style = $node->panelizer['page_manager']->display->content[$title_id]->style;
        }
        if (isset($title_style['style'])) {
          $title_style = str_replace('_', '-', $title_style['style']);
          $vars['title_style'] = $title_style;
        }
        break;
      }
    }

    // Add path and page_layout to variables for use in theme.
    $vars['node_path'] = $path;
    $vars['page_layout'] = $page_layout;
  }
}
// */

/**
 * Override or insert variables into the entity template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("entity" in this case.)
 */
/* -- Delete this line if you want to use this function
function uchciago_preprocess_entity(&$vars, $hook) {

}
// */

/**
 * Override or insert variables into the comment template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function uchicago_preprocess_comment(&$vars,$hook) {
  //  kpr($vars['content']);
}
// */

/**
 * Override or insert variables into the field template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("field" in this case.)
 */
/* -- Delete this line if you want to use this function
function uchicago_preprocess_field(&$vars,$hook) {
  //  kpr($vars['content']);
  //add class to a specific field
  switch ($vars['element']['#field_name']) {
    case 'field_ROCK':
      $vars['classes_array'][] = 'classname1';
    case 'field_ROLL':
      $vars['classes_array'][] = 'classname1';
      $vars['classes_array'][] = 'classname2';
      $vars['classes_array'][] = 'classname1';
    case 'field_FOO':
      $vars['classes_array'][] = 'classname1';
    case 'field_BAR':
      $vars['classes_array'][] = 'classname1';
    default:
      break;
  }

}
// */

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function uchicago_preprocess_maintenance_page(){
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  // uchicago_preprocess_html($variables, $hook);
  // uchicago_preprocess_page($variables, $hook);

  // This preprocessor will also be used if the db is inactive. To ensure your
  // theme is used, add the following line to your settings.php file:
  // $conf['maintenance_theme'] = 'uchicago';
  // Also, check $vars['db_is_active'] before doing any db queries.
}
// */
